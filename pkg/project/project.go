package project

import "gitlab.com/gkama-go/gtk-time-logging/pkg/timecard"

var projects []Project

//Project object
type Project struct {
	ID          string              `json:"id"`
	Name        string              `json:"name"`
	Description string              `json:"description"`
	Source      string              `json:"source"`
	WebsiteURL  string              `json:"website_url"`
	Timecards   []timecard.Timecard `json:"timecards"`
}
