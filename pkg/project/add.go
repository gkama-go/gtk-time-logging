package project

import "gitlab.com/gkama-go/gtk-time-logging/pkg/timecard"

//Add is used to add a project object to the db
func Add(project Project) {
	if contains(project) == false {
		projects = append(projects, project)
	}
}

//AddProjects is used to add an array of projects at once
func AddProjects(projects []Project) {
	for _, a := range projects {
		Add(a)
	}
}

//InitProjects initializes the in-memory projects used in this application
func InitProjects() {
	AddProjects([]Project{
		Project{
			ID:          "0b501b5a-5518-4285-8630-91c664edd0dc",
			Name:        "Learning Go",
			Description: "Learning Golang (Go) in my spare time",
			Source:      "Online",
			WebsiteURL:  "https://golang.org/",
		},
		Project{
			ID:          "c8539aad-bcf5-4b34-b5c2-3bd092ffb292",
			Name:        "Praticing C#/.NET Core",
			Description: "Practicing and expanding my C#/.NET Core skills in my spare time",
			Source:      "Online",
			WebsiteURL:  "https://docs.microsoft.com/en-us/dotnet/core/",
		},
	})

	ts := timecard.GetTimecards()

	//Add the timecards
	for i := 0; i < len(projects); i++ {
		for j := 0; j < len(ts); j++ {
			if ts[j].ProjectID == projects[i].ID {
				projects[i].Timecards = append(projects[i].Timecards, ts[j])
			}
		}
	}
}

func contains(p Project) bool {
	for _, a := range projects {
		if a.Name == p.Name {
			return true
		} else if a.ID == p.ID {
			return true
		}
	}
	return false
}
