package project

//Get is used to get a project by id
func Get(id string) Project {
	for i := 0; i < len(projects); i++ {
		if projects[i].ID == id {
			return projects[i]
		}
	}

	return Project{}
}

//GetProjects returns a list of all projects
func GetProjects() []Project {
	return projects
}
