package timecard

import "time"

//Add is used to add a project object to the db
func Add(timecard Timecard) {
	if contains(timecard) == false {
		timecards = append(timecards, timecard)
	}
}

//AddTimecards is used to add an array of projects at once
func AddTimecards(timecards []Timecard) {
	for _, a := range timecards {
		Add(a)
	}
}

//InitTimecards is used to initialize the timecards
func InitTimecards() {
	AddTimecards([]Timecard{
		Timecard{
			ID:          "c86bcfb8-3fc8-4ef0-a7c2-e7d3be49b561",
			ProjectID:   "0b501b5a-5518-4285-8630-91c664edd0dc",
			Date:        time.Now(),
			Description: "I learned how to use slices in Go!",
			Day:         "Thursday",
			Hours:       1,
		},
		Timecard{
			ID:          "165f2d8d-ba36-4e9d-a3df-7b5841f2b91e",
			ProjectID:   "0b501b5a-5518-4285-8630-91c664edd0dc",
			Date:        time.Now(),
			Description: "I learned how to use variables in Go!",
			Day:         "Thursday",
			Hours:       1.5,
		}, Timecard{
			ID:          "341c75f5-9c9c-46a4-8a24-99b2680e1ca7",
			ProjectID:   "c8539aad-bcf5-4b34-b5c2-3bd092ffb292",
			Date:        time.Now(),
			Description: "I made an effort to use more generics in C#. This helped me expand my knowledge in them!",
			Day:         "Thursday",
			Hours:       0.5,
		}, Timecard{
			ID:          "aee0cc04-1c67-4005-9556-a23cc5505bf7",
			ProjectID:   "c8539aad-bcf5-4b34-b5c2-3bd092ffb292",
			Date:        time.Now(),
			Description: "Expanded my knowledge in .NET Core API and DI",
			Day:         "Thursday",
			Hours:       2.5,
		},
	})
}

func contains(t Timecard) bool {
	for _, a := range timecards {
		if a.ID == t.ID {
			return true
		}
	}
	return false
}
