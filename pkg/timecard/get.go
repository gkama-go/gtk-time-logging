package timecard

//Get is used to get a timecard by id
func Get(id string) Timecard {
	for i := 0; i < len(timecards); i++ {
		if timecards[i].ID == id {
			return timecards[i]
		}
	}

	return Timecard{}
}

//GetTimecards returns a list of all projects
func GetTimecards() []Timecard {
	return timecards
}