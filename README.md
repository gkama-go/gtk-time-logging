# gtk-time-logging in Go
this project is a simple time logging application. the need for it was to log any time that I spent learning something new (in my initial case, Golang) or anything that I wanted to log my time for

- written in Golang (Go)
- uses `Gin`
- uses `GraphQL`

## overview
- there are 2 objects, a `Project` and a `Timecard`