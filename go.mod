module gitlab.com/gkama-go/gtk-time-logging

go 1.12

require (
	github.com/gin-gonic/gin v1.4.0
	github.com/graphql-go/graphql v0.7.8
)
