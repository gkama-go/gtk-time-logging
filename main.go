package main

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/graphql-go/graphql"

	"gitlab.com/gkama-go/gtk-time-logging/pkg/project"
	"gitlab.com/gkama-go/gtk-time-logging/pkg/timecard"
)

//GraphQL
//https://github.com/graphql-go/graphql/blob/master/examples/crud/main.go
var projectType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "Project",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type: graphql.String,
			},
			"name": &graphql.Field{
				Type: graphql.String,
			},
			"description": &graphql.Field{
				Type: graphql.String,
			},
			"source": &graphql.Field{
				Type: graphql.String,
			},
			"website_url": &graphql.Field{
				Type: graphql.String,
			},
			"timecards": &graphql.Field{
				Type: graphql.NewList(timecardType),
			},
		},
	},
)
var timecardType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "Timecard",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type: graphql.String,
			},
			"project_id": &graphql.Field{
				Type: graphql.String,
			},
			"start_date": &graphql.Field{
				Type: graphql.DateTime,
			},
			"end_date": &graphql.Field{
				Type: graphql.DateTime,
			},
			"description": &graphql.Field{
				Type: graphql.String,
			},
			"day": &graphql.Field{
				Type: graphql.String,
			},
			"hours": &graphql.Field{
				Type: graphql.Int,
			},
		},
	},
)

var queryType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "Query",
		Fields: graphql.Fields{
			"project": &graphql.Field{
				Type:        projectType,
				Description: "Get project by id",
				Args: graphql.FieldConfigArgument{
					"id": &graphql.ArgumentConfig{
						Type: graphql.String,
					},
				},
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					id, ok := p.Args["id"].(string)
					if ok {
						return project.Get(id), nil
					}
					return nil, nil
				},
			},
			"projects": &graphql.Field{
				Type:        graphql.NewList(projectType),
				Description: "Get product list",
				Resolve: func(params graphql.ResolveParams) (interface{}, error) {
					return project.GetProjects(), nil
				},
			},
			"timecard": &graphql.Field{
				Type:        timecardType,
				Description: "Get timecard by id",
				Args: graphql.FieldConfigArgument{
					"id": &graphql.ArgumentConfig{
						Type: graphql.String,
					},
				},
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					id, ok := p.Args["id"].(string)
					if ok {
						return timecard.Get(id), nil
					}
					return nil, nil
				},
			},
		},
	})

var schema, _ = graphql.NewSchema(
	graphql.SchemaConfig{
		Query: queryType,
	},
)

func executeQuery(query string, schema graphql.Schema) *graphql.Result {
	result := graphql.Do(graphql.Params{
		Schema:        schema,
		RequestString: query,
	})
	if len(result.Errors) > 0 {
		fmt.Printf("errors: %v", result.Errors)
	}
	return result
}

func main() {
	r := gin.Default()

	//initialize projects and timecards
	timecard.InitTimecards()
	project.InitProjects()

	r.GET("/projects", func(c *gin.Context) {
		c.JSON(http.StatusOK, project.GetProjects())
	})
	r.GET("/project/:id", func(c *gin.Context) {
		c.JSON(http.StatusOK, project.Get(c.Param("id")))
	})
	r.GET("/timecards", func(c *gin.Context) {
		c.JSON(http.StatusOK, timecard.GetTimecards())
	})
	r.GET("/timecard/:id", func(c *gin.Context) {
		c.JSON(http.StatusOK, timecard.Get(c.Param("id")))
	})

	//GraphQL
	r.GET("/graphql", func(c *gin.Context) {
		c.JSON(http.StatusOK, executeQuery(c.Query("query"), schema))
	})

	r.Run()
}
